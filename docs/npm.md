# NPM Packages & the GitLab Package Registry

## The Important Bits

When publishing an NPM package to the **project-level** package registry, there are just a few specific tidbits we need to worry about:

- Naming convention
- Version (kinda)
- Authentication

### Naming Convention

This is covered in detail within our [docs](https://docs.gitlab.com/ee/user/packages/npm_registry/#package-naming-convention), so I'll be brief here. The package we're publishing must be scoped to the root namespace of wherever it's being published, whether it be a group or a personal namespace.

The naming (and subsequently the scope) are defined in the `name` field of the `package.json` file. This field in the default `package.json` for this project looks like so:

```json
{
  "name": "@calebw/npm",
  ...
}
```

We've defined the scope as `@calebw/` and the name of the package itself as `npm`. The scope is set to the root namespace housing this project, and the name is whatever you would like the package name to be - `npm` for this example.

The scope we've defined here will be used by the NPM utility when looking to publish our package. When forking this project and running a pipeline to publish these example packages, you **do not** need to worry about manually editing this. The CI job responsible for this package will handle this for you - this is covered further in the Authentication section and the CI job overview below.

### Version

The version is less important, especially in the context of this example project, but there is one "gotcha" that's worth noting.

You **cannot** publish a package with the same name and version in a given scope. Doing so will result in a `403 forbidden` response from the GitLab Package Registry. The CI job responsible for this package will automatically come up with a random number to use as for the package version to try and avoid this - see the CI job overview for more details.

### Authentication

When you attempt to publish the package, the NPM utility will search for an `.npmrc` file and try and find the configuration within this file that corresponds to the scope you've defined for the package.

Since the default `package.json` for this project is scoped to `@calebw`, the `.npmrc` file would look like so:

```ini
@calebw:registry=https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/packages/npm/
//gitlab.com/api/v4/projects/${CI_PROJECT_ID}/packages/npm/:_authToken=${CI_JOB_TOKEN}
```

The `@calebw` above tells NPM to use this configuration for any packages scoped with `@calebw/`. This scope is then pointed to the relevant API endpoint for publishing NPM packages to the GitLab Package Registry, where a [CI/CD job token](https://docs.gitlab.com/ee/ci/jobs/ci_job_token.html) is used to authenticate.

This project does not house an `.npmrc` by default. Instead, it's created automatically within the CI job. This is covered in more detail below.

----

## CI Job Overview

Here's the CI job for publishing the NPM package:

```yaml
npm:
  image: node:lts-alpine3.14
  before_script:
    - apk update && apk add jq
  script:
    - cd npm/
    - export NAMESPACE="@${CI_PROJECT_ROOT_NAMESPACE}/npm"
    - export VERSION=$(( $RANDOM % 9 )).$(( $RANDOM % 99 )).$(( $RANDOM % 99 ))
    - jq -r '.name = env.NAMESPACE | .version = env.VERSION' package.json > package2.json && rm package.json && mv package2.json package.json
    - cat package.json
    - |
      {
        echo "@${CI_PROJECT_ROOT_NAMESPACE}:registry=${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/npm/"
        echo "${CI_API_V4_URL#https?}/projects/${CI_PROJECT_ID}/packages/npm/:_authToken=\${CI_JOB_TOKEN}"
      } | tee -a .npmrc
    - cat .npmrc
    - npm publish
```

After moving into the `npm` directory, we first set the `NAMESPACE` variable, which is used for the `name` field in the `package.json`:

```sh
export NAMESPACE="@${CI_PROJECT_ROOT_NAMESPACE}/npm"
# This would equate to something like NAMESPACE="@calebw/npm"
```

This makes use of the [predefined CI/CD variable](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html) `CI_PROJECT_ROOT_NAMESPACE` which is automatically set to the root namespace housing your project. This is followed by `/npm`, defining the name of the package we're publishing as `npm`.

We then define the `VERSION` variable:

```sh
export VERSION=$(( $RANDOM % 9 )).$(( $RANDOM % 99 )).$(( $RANDOM % 99 ))
# This would equate to something like VERSION=1.23.45
```

This is simply generating a random version to use within the `package.json`, grabing a random number between 0-9, and a random number between 0-99 for the last two decimals.

Using these variables, we make use of the `jq` utility to replace the `name` and `version` values in the `package.json`:

```sh
jq -r '.name = env.NAMESPACE | .version = env.VERSION' package.json > package2.json && rm package.json && mv package2.json package.json
```

After the `package.json` has been automatically configured to use the proper scope and set a random version, we setup our `.npmrc` file to authenticate with GitLab in order to publish the package:

```sh
|
      {
        echo "@${CI_PROJECT_ROOT_NAMESPACE}:registry=${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/npm/"
        echo "${CI_API_V4_URL#https?}/projects/${CI_PROJECT_ID}/packages/npm/:_authToken=\${CI_JOB_TOKEN}"
      } | tee -a .npmrc
```

Since this will heavily change between each namespace, it's easier to just create it in the pipeline rather than editing an existing file in the repository. As such, we make use of a handful of predefined CI/CD variables to set the scope, relevant project IDs and API endpoints, as well as the `CI_JOB_TOKEN` to authenticate before using `tee` to pipe it all into the `.npmrc` file.

With the above we've specified all the package details and configured NPM to properly authenticate with GitLab, leaving us with `npm publish` to push our package up to the GitLab Package Registry