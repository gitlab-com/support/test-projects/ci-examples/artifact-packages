# Generic Packages & the GitLab Package Registry

## The Important Bits

There isn't a lot to generic packages. These are just files of your choosing (compressed archives, binaries, etc) that are stored in the package registry with a name and version for controlled fetching. These are the important bits:

- Name & Version
- Authentication

### Name & Version

Unlike most other package types in the GitLab Package Registry, you won't encounter an error when attempting to publish a package with the same name and version of an existing package in the registry. Instead, the files uploaded will be placed along side those of the existing package.

Further, we don't define the name or version of Generic packages in any sort of configuration file. These are passed in when we make the API call to upload the packages following this format:

```txt
/api/v4/projects/:id/packages/generic/:package_name/:package_version/:file_name
```

We'll touch on this more in the CI job overview.

### Authentication

Since the upload is of generic packages is made by a direct API call, we don't have any sort of authentication/source configuration that specifies where we're uploading the packages and who we're authenticating as. Instead, these values are passed in with the call to the API.

----

## CI Job Overview

Here's the CI job for publishing the PyPI package:

```yaml
generic:
  image: curlimages/curl:latest
  script:
    - export VERSION=$(( $RANDOM % 9 )).$(( $RANDOM % 99 )).$(( $RANDOM % 99 ))
    - tar -czvf test.tar.gz generic/
    - 'curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file test.tar.gz "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/generic/$VERSION/test.tar.gz"'
```

First we export the `VERSION` variable for the package:

```sh
export VERSION=$(( $RANDOM % 9 )).$(( $RANDOM % 99 )).$(( $RANDOM % 99 ))
# This would equate to something like VERSION=1.23.45
```

This is simply generating a random version value and assigning it to the `VERSION` environment variable, which is latter referenced by the `curl` command responsible for calling the API. This grabs a random number between 0-9, and a random number between 0-99 for the last two decimals.

We then create an archive named `test.tar.gz` of the `generic/` directory, which just contains a text file:

```sh
tar -czvf test.tar.gz generic/
```

Finally, we use `curl` to upload the archive as a package to the generic package registry:

```sh
'curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file test.tar.gz "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/generic/$VERSION/test.tar.gz"'
```

We make use of [predefined CI/CD variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html) to automtically fetch the appropriate address for your package, and the `CI_JOB_TOKEN` to authenticate. The package name here is defined in the URL we're making the API call to, in this case it's simply named `generic`. If we were to change the name of the package to something like "binaries", the URL would look like this:

```sh
"${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/binaries/$VERSION/test.tar.gz"
```

We also use the `VERSION` environment variable created at the beginning of the job to define the version of this package. After this step is executed, the package is uploaded and ready to go.
